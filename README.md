# What

[David Pelayo's website](https://davidpelayo.com) made with HUGO and [cocoa hugo theme](https://github.com/nishanths/cocoa-hugo-theme/tree/master).

# How

## Docker

There is a `docker-compose.yml` file which relies on two Docker [images published to the Docker public registry](https://hub.docker.com/u/davidpelayo/).

## Hugo

[Hugo](https://gohugo.io/) technology is the one used on this website. Checkout the documentation to see how it works.
