+++
date = "2015-08-22T06:28:26-07:00"
draft = false
title = "this is me"
heading = "About"
aliases = [
    "/about-this-site/"
]

+++

He was born in 1988, loves running, craft beer, awesome high quality coffee, technology, reading,
bonobos, nature, hiking, mountains, sea, talking, people, women, love, languages,
thinking, drawing and doubting everything.

He speaks spanish, andalusian dialect (from the south of Spain), catalan, english, italian,
portuguese and understand a bit of others.

Grew up in Algeciras (14 kms far from Morocco, at the south of Spain) and started to develop his career in Alcalá de Henares, city of Miguel de Cervantes, in Madrid.

There he met many people: many of them, introduced him in different worlds which allowed him to learn, discover and expand his mind.

Since he started studying English a summer in Bournemouth, UK - rapidly felt the need of continuing
exploring other languages, cultures and places.

On each trip always met who today are probably the most important people in his life.

He was discovering Asia and Latinoamerica lately, and for now lives near to Barcelona and Tarragona, cities of Spain, Europe.

Lover of feminism, criticism, productivity, methodologies to analyse the emotional intelligence, among other topics.

He does love people, needing them so much; at the same time, though, he's quite independent and likes to be alone and enjoying silence: meditating
and thinking are also activities he practise.

Last but not least, He'd run two businesses already. The first one executed successfully selling
custom designed cardboard houses for kids - Mondoko.

Second one was in 2017 - IT consultancy services for companies - which ended in November 2017 successfully.
