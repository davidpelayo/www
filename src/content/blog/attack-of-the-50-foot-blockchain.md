---
title: "Attack of the 50 Foot Blockchain"
date: 2017-11-27
permalink: "attack-of-the-50-foot-blockchain"
---
From a tweet, to a [book](https://www.goodreads.com/book/show/35525995-attack-of-the-50-foot-blockchain), to a review.

# A tweet

{{< tweet 926322295936753664 >}}

# A book

It started here.

Reading this tweet, I decided to buy the book and introduce myself a bit deeper into the world of Bitcoin.

The author, David Gerard, seems to be detractor of cryptocurrencies.

# A review

It just took me a few days to read this book entirely.

Why? Half of the book are references and links to websites and articles to contrast his own opinions.

The introduction and the historical chain of events is a well explained section of this book.

This books uncovers the foreword, ongoings and present state of such experimental technologies.

He stands up for Bitcoin as scam, full of cons and fraud. An experiment it can't practically work and won't work.

It's an easy to follow and read book, but clearly biased from the vision of the author, who tends to defend
that blockchain technology is kind of an useless invention - [tell the same to the ones they just got out when the value arised $10k these days](https://www.reddit.com/r/Bitcoin/comments/7fvoxh/guys_i_am_out/).

However, it's a fun read with a few memorable stories and anecdotes using sarcasm only when it's time to.

Plenty of examples and deals that happened in the community, it certainly gives a good overview
and an initial idea of what bitcoin is and what's the world behind.

The truth is, though, that the world behind is just people.

People and their constant willing of making money, after all.

My conclusion is I totally recommend to read this book. Additionally, the sooner, the better. Because this topic is evolving too quick.

# Further reading

I'm listing the further reading section of the book just for practicality.

And, at the same time, if you're interested - fair enough.

- _Memoirs of Extraordinary Popular Delusions and the Madness of Crowds_ by Charles Mackay was the first published in 1841 and remains the best book available on the economic bubble thinking. [[Reference](http://www.gutenberg.org/ebooks/24518)].
- Nathaniel Popper's book [_Digital Gold: Bitcoin and the Inside Story of the Misfits and Millionaires Trying to Reinvent Money_](http://www.nathanielpopper.com/about-digital-gold.html) is an excellent history of Bitcoin and the players to 2014.
- David Golumbia's [_The Politics of Bitcoin: Software as Right-Wing Extremism_](https://www.upress.umn.edu/book-division/books/the-politics-of-bitcoin) is a short but very useful academic survey that traces just where the Bitcoin cluster of crank political and economic ideas sprang from.
- Izabella Kaminska regularly discusses Bitcoin and blockchains (and "Blockchain") in the _Financial Times_, both in the main paper and her blog at _FT Alphaville_. The author has found her work a powerful
and effective antidote to business bafflegab Blockchain hype in real-world usage. Matt Levine does [similarly good work](https://www.bloomberg.com/view/contributors/ARbTQlRLRjE/matthew-s-levine) at Bloomerg.
- The RationalWiki.org [article of Bitcoin](http://rationalwiki.org/wiki/Bitcoin) has come along nicely since 2011.

# A conversation that blew my mind

I also recommend to watch this video if you want to have an introduction on what's going on on this world.

{{< youtube IrSn3zx2GbM >}}
