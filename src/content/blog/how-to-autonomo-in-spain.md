---
title: "How to work as autonomo or freelance in Spain"
date: 2017-11-24
permalink: "how-to-autonomo-in-spain"
---
People wonder how to start being `autónomo` or how this labor figure works in Spain.

`Autónomo` __stands for `self-sufficient`: working as freelance/contractor__.

So instead of letting the employeer being responsible for tax liabilities and other related topics, you manage everything by your own.

This discussion commonly go bound to the legal system the country is attached to.

Of course it can become a complex deal to look into, depending on certain particulars.

But let's be a bit straightforward and suppose __you already know you want to work as freelance in Spain and you don't know how to start__.

# State of the Art of Autonomos in Spain

Rules on this topic are strict.

Regardless your earnings are high or low or casual, you must register and pay, as everybody.

The time when Spain was a tax haven ended a few years ago - at least for regular people.

If you work as IT or Engineering consultant, teacher or anything that requires running your own small
business invoicing customers - then you must register as Autonomo.

The pros and cons on having an S.L. Company vs being Autonomo can be discussed in a different post.

# Foreigners from outside the EU

People from outside the European Union, over 16 years old, willing to work in Spain
as freelancers must obtain the proper administrative authorization first, as per to obtain the [NIE](https://www.spanishpropertyinsight.com/legal/spanish-nie-numbers/).

## Asking for the "temporal residence" and freelance authorization

### Requirements

- Staying legally here.
- Absence of criminal records certificate.
- Expenses estimation documentation specifying how the first year of the activity will look like,
covering accomodation and basics to live here.
- The association official certificiation of her/his activity to demonstrate the ability to
work as said.
- Investment plan.
- Recommendationos or demonstrated qualifications.

### How?

At the spanish consular office, the request should be presented - [this document in spanish](http://www.exteriores.gob.es/Consulados/SANFRANCISCO/en/ConsularServices/Documents/visas/EX01.pdf)

By the way, consular offices can be explored at [this official website](http://www.exteriores.gob.es/Portal/en/Paginas/inicio.aspx).

Documents to present with this request:

- Valid Passport, not expired.
- Absence of criminal records certificated issued by the source country.
- Sanitary certificate to check everything is ok.
- The association official certification saying you can actually do that job and you are qualified to do it.
- Investment plan.
- Project plan.
- If you need any extra certificate or license to do your activity physically, add them too.

At this point the procedure is started.

If approved, they'll tell you __how much to pay first__ (smart eh?) and then you ask for the VISA.

And now is when you can actually do the actual steps a regular spaniard would do to start the activity.
And it must be on site - for this case.

Any other official application documents could be found at [this link](http://extranjeros.empleo.gob.es/es/ModelosSolicitudes/).

Use Google Translator tool to translate the page if you don't understand spanish - or just ask at the bottom right corner of this website.

# Signing up as Autonomo

There are mainly two parts on the process of registering.

1. Registering with the tax office (Agencia Tributaria, formerly Hacienda).
2. Joining the Autonomo Social Security System (Regimen Especial de Trabajadores Autónomos - RETA).

## Requirements

- To have your NIE.
- To have a Spanish bank account.
- Work permit if you are from outside the EU.

__Some of the concepts you must have clear in mind before acting as freelance in Spain__:

- As Autonomo you are the only responsible one of informing the Ministery and legal system of Spain when you start
your activity or when it ends. This is a requirement and must be informed precisely on each change.
- If there is any change on your activity while being Autonomo, you must notify it.
- If for whatever reason you already work for a company in Spain, at the same time, you can be Autonomo so doing consultancy or issuing invoices
to your clients is perfectly compatible.

## Registering with the Tax Offices

The Agencia Tributaria or Hacienda expects you to be registered as a resident taxpayer - to do this, you can fulfill the Modelo 036 form - or Modelo 037. For the matter
of this article, both work out the same.

This form asks you to specify who you are, where you live, what the business you'll run look like and where you'll
be paying VAT (IVA in spanish).

Paying VAT or not depends basically on the type of product or service you charge for. The nature of your activity.

Some activities are exempt from VAT. Or even invoicing companies overseas, on certain cases, are exempt too.

A great explained guide on how to register on these steps can be found [here](https://getquipu.com/blog/como-darse-de-alta-como-autonomo-paso-a-paso/).

[Quipu](https://getquipu.com/) is a great SaaS product that allow autonomos to manage accounting and other tasks with some sort of automatism.

## Registering with the Social Security

Only once you already registered for the purposes of tax and VAT, you can and must register for social security purposes. You may need to wait 24-48h, after your registration to Hacienda until both administrations synchronize their data to start this registration.

Another form acknowledging your autonomo classification.

Depending on the category chosen for your activity, there are different social security paments.

The usual payments are over __€267 per month__. However, there have been changes to encourage
people to register as autonomo paying a reduced quantity of __€50__ for the first 12 months of self employment.

Nobody gives anything for free. Do not think of this reduction as a white-paper advantage. Later, they claim some percentage back.

## Other obligations

- Quarterly tax and VAT returns.
- Monthly social security payments up to date (this is automatic, so you don't worry about it!).
- Issue drawn-up invoices.
- Accounting records according to the legal standards of Spain (this is where Quipu comes in!).
- Retaining __15%__ of any invoice when invoicing other autonomo or business in Spain. Then is paid to the Agencia Tributaria
as advanced income tax and then credited when quarterly income tax returns are made. For freelancers
with less than 3 years trading the rate is __7%__.

## Changes in 2017

In November 2016, an [government proposal](http://www.congreso.es/public_oficiales/L12/CONG/BOCG/B/BOCG-12-B-56-1.PDF) proposd to change the tax laws
for self employed workers to encourage, as I said before, more people to become _autonomo_. This new tax laws covers:

- Flat rate of __€50__ per month for the first 12 months. After 12 months it gets increased.
- Up to __50% of petrol expenses__ can be claimed back.
- Up to __20% of electricity, gas and water__ costs can be claimed back if working from home.
- Penalties for freelancers who are __late paying the social security payments__.

Think of this: The spanish legal and tax system wants you to pay, always, and without complaining.

One of the big boys in Spain, even bigger than the Agencia Tributaria, is the
Social Security system. Their numbers are __huge__.

If there is anyone in this world you wouldn't like to fight against, would be the Social Security of Spain - even though they are at the same time
quite an imperfect and unproductive organisation, from the structural and functional points of view.

# Who to ask

If you have any doubts regarding all this to land in Spain and start working, maybe because you love the sun
or just be partying, drop me a message.

__There are quite a few different types of accountants or _gestores_ (agents) who are willing to take your money, as much as they can, for doing nothing__.
