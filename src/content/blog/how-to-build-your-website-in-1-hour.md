---
title: "How to build your website in 1 hour"
date: 2017-11-21
permalink: "how-to-build-your-website-in-1-hour"
---
# Idea
You want a personal website, but you don't want to spend days
designing, coding and making it perfect: simply because you
have more important stuff to do now.

Fair enough.

You know there are plenty of products you could
use to build your own website.

Simple and straightforward - some of them
are CMS such as [Wordpress](https://wordpress.com/) - or others as [carrd.co](https://carrd.co/) with
wonderful user experiences built in.

I wanted something minimal enough to provide content first.

# Hosting
Do not mess around with hosting.

Create the simplest and cheapest
[Digital Ocean](https://digitalocean.com) droplet, which is just **$5/month** and that
is enough for now. Of course, we talk of Linux servers - Ubuntu is ok.

There is a quicker and easier alternative in which you wouldn't need any hosting.

Services like [Netlify](https://netlify.com) manage static sites building, publishing and replication throughout CDN for free.

I haven't still integrated this site with Netlify, though, because they aren't supporting the latest version of [Hugo](https://gohugo.io).

I have pushed a bit for this update on [Netlify github repo - upvote or leave a comment if you want to use it too](https://github.com/netlify/build-image/issues/93).

# Domain name

I recommend [Namecheap](https://namecheap.com) because it regularly
includes 1 year subscription of WhoisGuard for not more than 9 €.

# Technologies

Use some existing technology which makes your life easier.

Searched a bit, and since I wanted to deploy my website easier and faster, I knew
I needed to use Docker technology for that.

In addition, using [Hugo](https://gohugo.io) technology makes your life easier
when it comes to publish content.

# How to
**I'm assuming you are familiar with the technologies described above,
so that you don't have to struggle much to use them productively.**

1. [Clone this github repo](https://github.com/davidpelayo/www) in your
personal website development folder.
2. Access your droplet and generate a [Let's Encrypt](https://letsencrypt.org/getting-started/) certificate so you could have `https` right away.
3. Write your info in the `docker-compose.yml`

```yaml
version: "2.1"

volumes:
  data:

services:
  hugo:
    image: davidpelayo/nginx-unit-hugo:0.30.2
    environment:
      - NGINX_UNIT_HOSTS=<your_domain_name>
      - NGINX_URL_PREFIX=/
      - HUGO_REPO_URL=<the_cloned_repo_url>
      - HUGO_REPO_SECRET=${WWW_TOKEN}
      - HUGO_THEME=cocoa
      - HUGO_SOURCE_FOLDER=/src
    volumes:
      - data:/opt/container/shared

  proxy:
    image: davidpelayo/nginx-host:1.12.2
    links:
      - hugo
    ports:
      - "443:443"
      - "80:80"
    volumes:
      - data:/opt/container/shared
      - /etc/letsencrypt:/etc/letsencrypt
      - /home/me/dhparam.pem:/etc/ssl/dhparam.pem
```

4. For quicker releasing, for now, access your droplet via SSH and sync your cloned
repo in a concrete folder.
5. Run the `docker-compose.yml` and you must be live.

Once you complete these steps, take care about other topics on your droplet.

Digital Ocean provides with tons of guidelines on how to setup your droplet.
For instance, [this is a good one](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04).

# This site's repo
If you want to collaborate with this website, or just ask a doubt or raise an issue,
find the code on [this repo](https://github.com/davidpelayo/www/issues) and participate!
