---
title: "DRTS & i2CAT"
date: 2012-03-01
permalink: "drts"
---
Coworked for [DRTS](http://www.drts.co.uk/) in [i2cat Foundation](http://www.i2cat.net/) for a few years.

DRTS's [DACORD](https://www.dacord.co.uk/) product in partnership with [Milliman UK](http://uk.milliman.com/) was developed as a research
project first - then became a product itself due the complexity and advantages of such platform.

Architectured using [WSO2 Stratos](https://wso2.com/cloud/stratoslive/) (now the retired [Apache Stratos](http://stratos.apache.org/)) to provide
high availability, auto scaling and scalable dynamic load balancing.

The toolset allow to analyse big datasets, stablish connections in data and find patterns, in order to make predictions.

The visualisations layer presents data using [BackboneJS](http://backbonejs.org/) and [D3](https://d3js.org/).
