---
title: "IBM & AtSistemas"
date: 2010-05-01
permalink: "ibm"
---
Coworked with one of the [AtSistemas](https://www.atsistemas.com/es)' clients - IBM Barcelona for a few years.

## [Gas Natural](https://www.gasnaturalfenosa.es/) Spain platform

Tailored CRM platform, developed using J2EE and [Adobe Flex 3](https://en.wikipedia.org/wiki/Apache_Flex#Adobe_Flex_3) technologies.

Once upon a time [Apache Flex was Adobe Flex](https://en.wikipedia.org/wiki/Apache_Flex) and
big energetic sector players such as Gas Natural believed their high rated old-schoolish technology advocates to adopt such technologies.

Some businesses' web platform frontends were being reinvented, again. But this time, using
a not SEO-friendly, not semantic, not maintainable nor scalable technology.

But who knows how the future looks like, right?

## [Galp Energia](http://www.galpenergia.com/EN/Paginas/Home.aspx) Spain platform

This company has been using [SAP](https://www.sap.com/index.html) for years.

One of its utilities, [SAP IS-U](https://en.wikipedia.org/wiki/SAP_IS-U) was meant to be reused
throughout the Web. And there is where SAP UCES comes in.

Using the SAP Utility Customers E-Services Galp Energia's customers' online platform was re-engineered.

This time at least the front was limited by the standards and browser vendors of such times, only.
