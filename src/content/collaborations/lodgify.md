---
title: "Lodgify"
date: 2015-09-01
permalink: "lodgify"
---
"Your vacation rental website, your rules"

This is the main message [Lodgify](https://lodgify.com) - SaaS company - spreads on the internet.

They provide a web based horizontal platform with tools such as:

- Website builder and design
- Booking system
- Channel manager
- Reservation system

With a few thousand customers made with ❤️ from Barcelona. This SaaS allows you
to create, for instance, your own "AirBnb" platform, in case you manage multiple properties.

Just a few high qualified professionals have delivered an outstanding tool accessible now, from any device, anywhere in the world.
