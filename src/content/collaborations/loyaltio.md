---
title: "Loyaltio"
date: 2016-12-01
permalink: "loyaltio"
---
[Loyaltio](http://loyaltio.com/), with HQ in UAE and Barcelona, develops custom cutting-edge software,
using techniques from different fields of the IT.

Among others, technologies cover the IoT, Data Visualization and Analytics, Geolocation,
Systems Integration, Visual Recognition Technologies and BI & Big Data.

Co-working with clients based in UAE is a amazing experience. Their way they see, analyse and understand the world
and provide solutions for it is, undoubtedly, an experience any consultant might need to have.

Successfully delivered a couple of projects in production now.
