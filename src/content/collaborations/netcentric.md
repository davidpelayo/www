---
title: "Netcentric"
date: 2013-09-01
permalink: "netcentric"
---
[Netcentric](https://netcentric.biz) is a Adobe Premier Solution Partner specialized in Adobe Analytics,
Adobe Campaign and Adobe Experience Manager (formerly CQ5).

Coworked with [Netcentric](https://netcentric.biz) in some web tailored solutions for its clients.

Among others, the Intranet and Internet platforms of one of the biggest banks in the world, based in Switzerland.

Following the latest web standards, solutions were developed focused on certain vendor browser versions, such as IE8.

Clients of this company can be checked out at [this link](https://www.netcentric.biz/what-we-do.html#clients). For NDA reasons, they can't be mentioned here.

When you work with german and swiss people, you realize how productivity and well-organized teams make the difference when it comes to deliver solutions and make clients happy.
