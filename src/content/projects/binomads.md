---
title: "binomads."
date: 2017-05-01
permalink: "binomads"
---
[binomads.](https://binomads.com) is been a consultancy company founded by [Javier Segura](https://twitter.com/jsegura) and [David Pelayo](https://twitter.com/ddpelayo) in 2017.

Binomads has successfully offered consultancy services and worked with international clients, developing tailored
cloud solutions for different entites around the globe.

For more information regarding this company, [check out the website](https://binomads.com).
