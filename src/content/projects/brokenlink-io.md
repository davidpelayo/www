---
title: "brokenlink.io"
date: 2017-11-02
permalink: "brokenlink-io"
---
![](/img/brokenlink-io.png)

[brokenlink.io](https://brokenlink.io) is simple and fast broken links checker API.

Use it for free and know how links on a website are resolved and why. Links availability
has a big impact on SEO, specially when positioning of a website depends on customers' CMS content.

Integrate it in your CI system and validate your links from a programmable perspective.

Launched on the 2nd November 2017.

[Upvote our Product Hunt post](https://www.producthunt.com/posts/brokenlink-io)
