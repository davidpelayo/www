---
title: "Digital legacy"
date: 2017-10-18
permalink: "digital-legacy"
---
[Digital Legacy](https://legacy.binomads.com/) is a [side project](https://en.wikipedia.org/wiki/Side_project)
and a proof of concept born from the idea of what's going to happen with our "digital legacy" once we die.

It answer a question like: How my loved ones can easily access to my online digital assets if they
don't know my master password, encrypted using 1Password?

![](/img/digital_legacy.png)

The proof of concept has been developed using a Serverless architecture, with Amazon Web Services.
Concretely, operations works with [Serverless framework](https://github.com/serverless/serverless).

The [Product Hunt Digital Legacy publication](https://www.producthunt.com/posts/digital-legacy) performed
well. Many *hunters* upvoted it.

{{< tweet 920421522207944705 >}}
