---
title: "Hacker News articles subscription Chrome plugin"
date: 2017-10-10
permalink: "hn-notifier"
---
Accessing in a daily basis to [Hacker News](https://news.ycombinator.com) allow
tech and non-tech people know what's going on in occidental countries, mostly. But,
most importantly, USA.

One of the most important aspects of this collaborative news aggregator is, undoubtedly,
the incoming traffic and participation.

When you read threads, comments are fundamental key pieces when it comes to build
an idea of the linked article.

Nevertheless, the platform doesn't comes with the possibility of subscribing the threads,
in a way a user could see what's being commented inside. And here there is our need.

The [HN Notifier Chrome Plugin](https://www.producthunt.com/posts/hn-notifier-for-chrome) allows you
to subscribe any threads, notifying when new comments are posted.
