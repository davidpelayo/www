---
title: "lomenú"
date: 2017-06-16
permalink: "lomenu"
---
![](/img/lomenu.jpg)

[Lomenú](https://lomenu.com) is a [*mobile-first*](https://en.wikipedia.org/wiki/Responsive_web_design) platform
which aggregates restaurants and their detailed menus. It's currently focused on Barcelona's ones.

Unlike other platforms online, [Lomenú](https://lomenu.com) allows you to focus on the
gastronomic experience, concretely the menu, its dishes, ingredients and prices. But,
most importantly, you're also able to find restaurants by searching for food names,
dishes or ingredients.

This became one of the first binomads' ideas launched.
