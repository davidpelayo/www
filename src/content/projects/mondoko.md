---
title: "mondoko"
date: 2011-08-01
permalink: "mondoko"
---
![](/img/mondoko_00.jpg)

[Mondoko](https://facebook.com/mondoko) was launched and executed succesfully as a custom board-designed
small houses for kids.

Why not designing, creating and making low cost cardboard houses for letting
children play with them? A house costed 10 €.

Possibilities were infinite. Painting, cutting and pasting over the roof, playing, creating
a little world inside, decorating, joining many of them to create neighborhoods...

A project planned for one year which sold 5k units of Mondoko houses.

The project raised 50k € in less than 12 months - and it was a side project.

![](/img/mondoko_01.jpg)

![](/img/mondoko_02.jpg)

![](/img/mondoko_03.jpg)
