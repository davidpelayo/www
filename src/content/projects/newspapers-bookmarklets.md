---
title: "Digital media signup remover bookmarklets"
date: 2017-10-01
permalink: "newspapers-bookmarklets"
---
What happens when you're browsing in digital media and suddenly signup forms pop up asking
for subscription, payments or logging in it?

It's annoying; you leave the website, or simply you sign up and keep reading.
Or you could even pay for it so they won't bother you again.

This is the users' *conversion*, from a marketing perspective.

Problem is, an almost forced *conversion* (by the way, not recommended by *White Hat UX* methods)
is a practise we doesn't like. It's at this point when we decide to develop *bookmarklets*
in charge of deleting and hiding them, while browsing.

The next version of these tools is a browser plugin doing the work behind the scenes.
As per *AdBlockers* do to avoid ads showing up.

[Here the Github repo](https://github.com/binomads/signup-remover).
