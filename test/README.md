# Data needed for localhost running

Here place two folders:

`dhparam.pem` - empty is ok.
`letsencrypt` - folder with the letsencrypt data, the same with basic files needed as per in your server `/etc/letsencrypt`

How to run it:

```
docker-compose down -v
docker-compose up
```
